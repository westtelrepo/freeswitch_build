#!/bin/bash

# error out and exit this script if any command errors
set -e

readonly STAGING="$(pwd)/staging"
readonly ROOT="$(pwd)"

# so FreeSWITCH can find its libraries that we built in the container
export PKG_CONFIG_PATH="/opt/freeswitch/lib/pkgconfig"

cd /usr/local/src/stack-20.21.2

# do WestTel patches
patch -t -p0 -i "$ROOT/experient_flashook.diff"
patch -t -p0 -i "$ROOT/westtel_cseq.diff"

echo "applications/mod_commands
applications/mod_av
applications/mod_conference
applications/mod_curl
applications/mod_db
applications/mod_dptools
applications/mod_enum
applications/mod_esf
applications/mod_expr
applications/mod_fifo
applications/mod_fsv
applications/mod_hash
applications/mod_httapi
applications/mod_sms
applications/mod_spandsp
applications/mod_valet_parking
applications/mod_voicemail
codecs/mod_amr
codecs/mod_b64
codecs/mod_g723_1
codecs/mod_g729
codecs/mod_h26x
codecs/mod_opus
databases/mod_pgsql
dialplans/mod_dialplan_asterisk
dialplans/mod_dialplan_xml
endpoints/mod_loopback
endpoints/mod_rtc
endpoints/mod_rtmp
endpoints/mod_skinny
endpoints/mod_sofia
#endpoints/mod_verto
event_handlers/mod_cdr_csv
event_handlers/mod_event_socket
formats/mod_local_stream
formats/mod_native_file
formats/mod_png
formats/mod_sndfile
formats/mod_tone_stream
languages/mod_lua
loggers/mod_console
loggers/mod_logfile
loggers/mod_syslog
say/mod_say_en
xml_int/mod_xml_cdr
xml_int/mod_xml_rpc
xml_int/mod_xml_scgi" > modules.conf

echo "=============== BOOTSTRAP ===================="
time ./bootstrap.sh -j

echo "=============== CONFIGURE ===================="
# -Wno-error because FreeSWITCH errors on warnings by default
# --enable-portable-binary is a FreeSWITCH-ism because they enable unportable optimizations/etc by default
#   without this, we can't compile on one machine and reliably run it on another
# we set up the directories such that /opt/freeswitch is the binaries and libraries---upgrades are a simple
#   replace and restart
# otherwise we put things in standard places
time ./configure CFLAGS=-Wno-error \
	--enable-portable-binary \
	--disable-core-libedit-support \
    --disable-libvpx \
	--prefix=/opt/freeswitch \
	--localstatedir=/var/ \
	--datarootdir=/usr/local/share \
	--runstatedir=/run \
	--with-logfiledir=/var/log/freeswitch \
	--sysconfdir=/etc

echo "================ MAKE ========================"

time make

# make check would be nice, needs libtap though (not in Debian)

echo "================ INSTALL ====================="

mkdir -p $STAGING/opt
# include the libraries that were built in the container
cp -a /opt/freeswitch $STAGING/opt/
# DESTDIR is a autotools-ism, allowing us to place the result in a different place in the filesystem
# so we can package it up without requiring this script to run as root
make install DESTDIR=$STAGING
make cd-sounds-install cd-moh-install DESTDIR=$STAGING

# FS version includes 32/64 bit, so we don't add it on ourselves
# +wt1 because we patch it, ~deb9 because this is for debian stretch
# WARNING: this also needs to be changed in Jenkinsfile
# readonly VERSION="$(LD_LIBRARY_PATH=$STAGING/opt/freeswitch/lib $STAGING/opt/freeswitch/bin/freeswitch -version | cut -d ' ' -f 3)+wt1~deb9"
readonly VERSION="20.21.2-deb9"

echo "============== PACKAGING ====================="

# --mtime is the time of the freeswitch commit
# TODO: figure this out automatically
# https://reproducible-builds.org/docs/archives/
readonly C_DATE="2021-02-26T14:17:16-07:00"

cd $STAGING/opt
# `dpkg --print-architecture` will print amd64 in a 64-bit container and i386 in a 32-bit container
mv freeswitch freeswitch-$VERSION-$(dpkg --print-architecture)-wt1
tar --sort=name --mtime="$C_DATE" --owner=0 --group=0 --numeric-owner -cjf $ROOT/freeswitch-opt-$VERSION-$(dpkg --print-architecture)-wt1.tbz freeswitch-$VERSION-$(dpkg --print-architecture)-wt1

# package up share differently, we don't need it for upgrades
cd $STAGING/usr/local/share
mv freeswitch freeswitch-share-$VERSION-wt1
tar --sort=name --mtime="$C_DATE" --owner=0 --group=0 --numeric-owner -cjf $ROOT/freeswitch-share-$VERSION-wt1-$(dpkg --print-architecture).tbz freeswitch-share-$VERSION-wt1
