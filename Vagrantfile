# -*- mode: ruby -*-
# vi: set ft=ruby :

# after bringing the machine up, you will need to go to http://localhost:8010/ and set it up
#
# `vagrant ssh` so you can get the setup password, do `sudo cat $filename`
# install suggested plugins, wait
# create an admin user (do whatever you will remember, do `something@invalid` for email)
# leave Jenkins URL as is
#
# after install
# go to "Manage Jenkins" -> "Manage Plugins", search for "Blue Ocean" and "Docker Pipeline" under available and install it, wait
# check "Restart Jenkins when installation is complete and no jobs are running"
# go to home page, click "Open Blue Ocean"
# "Create New Pipeline"
# "Git"
# Repository URL is /vagrant
# ignore credentials, just click "Create Pipeline", wait
#
# it will probably do a first build automatically. You can also stop builds and start builds
# (go to "branches") manually.

Vagrant.configure("2") do |config|
  config.vm.box = "debian/contrib-stretch64"

  # disable the vagrant-vbguest plugin, doesn't work for some reason
  config.vbguest.auto_update = false

  config.vm.provision "shell", inline: <<-SHELL
    if [ ! -f /etc/first_run_done ]; then
      export DEBIAN_FRONTEND=noninteractive
      apt-get update
      apt-get -y install dirmngr
      echo deb http://ppa.launchpad.net/ansible/ansible/ubuntu trusty main >> /etc/apt/sources.list
      apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 93C4A3FD7BB9C367
      apt-get update
      apt-get -y install ansible
      touch /etc/first_run_done
    fi
  SHELL

  config.vm.define "jenkins", primary: true do |jenkins|
    jenkins.vm.box = "debian/contrib-buster64"
    jenkins.vm.hostname = "jenkins"

    jenkins.vm.network "forwarded_port", guest: 8080, host: 8010, host_ip: "127.0.0.1"

    # don't allow jenkins to write to /vagrant
    jenkins.vm.synced_folder ".", "/vagrant", owner: "vagrant", group: "vagrant", mount_options: ["dmode=775,fmode=664"]

    jenkins.vm.provider "virtualbox" do |vb|
      # you will probably want to shrink these if your computer isn't this powerful
      vb.memory = 16384
      vb.cpus = 24
    end

    jenkins.vm.provision "ansible_local" do |ansible|
      ansible.playbook = "jenkins.yml"
    end
  end

  config.vm.define "deb9", autostart: false do |deb9|
    deb9.vm.hostname = "deb9";
  end
end
