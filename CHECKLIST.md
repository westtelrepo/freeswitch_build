Download the source code for the FreeSWITCH revision, unaltered, and put it into a .tar.gz format.
(the signalwire stack will have a .tar.gz download you can use unaltered)

The file will likely be called `stack-$VERSION.tar.gz`. Put it in the root of this repository.

Delete the older source code in the repository (it is still kept in the repo history).

Update the references in `README.md`.

Update the references in `Dockerfile.*` (all files).

Update the references in `build.sh`:

* `cd /usr/local/src/stack-$VERSION`
* `readonly VERSION=`

Update the FreeSWITCH libraries that are referenced in `Dockerfile.*` (if needed).

`Dockerfile.*` should only have one difference (the first `FROM x` line).

Update the FreeSWITCH versions in `Jenkinsfile`.

I recommend using vagrant (do `vagrant up` and follow the instructions in `Vagrantfile`)
to create a local Jenkins instance to test with. You will then be able to `git commit` and
test changes locally and then use `git commit --amend` to keep the Git history sane before
finalizing with `git push`.
