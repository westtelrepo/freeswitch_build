pipeline {
    agent none
    stages {
        stage('Build') {
            parallel {
                stage('Build on stretch/amd64') {
                    agent {
                        dockerfile { filename 'Dockerfile.stretch' }
                    }
                    steps {
                        sh './build.sh'
                    }
                    post {
                        success {
                            archiveArtifacts artifacts: 'freeswitch-opt-*.tbz', fingerprint: true
                            stash name: 'amd64', includes: 'freeswitch-share-*.tbz'
                        }
                        cleanup {
                            cleanWs()
                        }
                    }
                }

                stage('Build on stretch/i386') {
                    agent {
                        dockerfile { filename 'Dockerfile.i386' }
                    }
                    steps {
                        sh './build.sh'
                    }
                    post {
                        success {
                            archiveArtifacts artifacts: 'freeswitch-opt-*.tbz', fingerprint: true
                            stash name: 'i386', includes: 'freeswitch-share-*.tbz'
                        }
                        cleanup {
                            cleanWs()
                        }
                    }
                }
            }
        }

        // we only want one share tarball, so we check that both share tarballs created above are bit-for-bit identical
        // (as they should be) and just use one of them
        stage('Check share') {
            agent any
            steps {
                unstash name: 'amd64'
                unstash name: 'i386'
                // diff gives a non-zero error code if the arguments differ
                sh 'diff *.tbz'
                sh 'mv freeswitch-share-20.21.2-deb9-wt1-amd64.tbz freeswitch-share-20.21.2-deb9-wt1.tbz'
            }
            post {
                success {
                    archiveArtifacts artifacts: 'freeswitch-share-20.21.2-deb9-wt1.tbz', fingerprint: true
                }
                cleanup {
                    cleanWs()
                }
            }
        }
    }
}