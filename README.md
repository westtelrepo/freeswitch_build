If you need to upgrade the FreeSWITCH build follow `CHECKLIST.md`.

`Vagrantfile` (which references `jenkins.yml`) are used to build a local Jenkins instance to test changes to this repository.
Vagrant may be downloaded from https://www.vagrantup.com/downloads. After vagrant is setup, run `vagrant up` in a
checked-out repo and then follow the instructions in `Vagrantfile` to finish setting up the Jenkins instance.

`Jenkinsfile` describes the Jenkins pipeline. Currently a 32-bit and a 64-bit version is built,
and share is checked to be the same between them.

`stack-20.21.2.tar.gz` is unedited source code from Signalwire's repository https://github.com/signalwire/stack/releases/tag/v20.21.2 (requires login).

`Dockerfile.*` (referenced by `Jenkinsfile`) describe the build environments (there are currently 2, 64-bit and 32-bit Debian Stretch).
FreeSWITCH source code is also unpacked here.

`build.sh` (referenced by `Jenkinsfile`) describes the actual build process, including applying patches (`*.diff`) to FreeSWITCH source code.

Builds are mostly reproducible at this point. (https://reproducible-builds.org/)

# Documentation

## Vagrant

Intro: https://www.vagrantup.com/intro
Getting Started: https://learn.hashicorp.com/collections/vagrant/getting-started
Documentation: https://www.vagrantup.com/docs

## Jenkins

https://www.jenkins.io/doc/book/pipeline/jenkinsfile/
https://www.jenkins.io/doc/

## Docker

https://docs.docker.com/get-started/overview/

## Ansible

https://docs.ansible.com/
